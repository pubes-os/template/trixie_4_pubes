# trixie
Salt formulae to clone `debian-12-minimal` TemplateVM & perform an in-place dist upgrade to [trixie](https://www.debian.org/releases/trixie) for Pubes OS

![](trixie.png){width=100%}

-------------

### Intro

- Create TemplateVM
- Made for Pubes 4.1+
- BYOH (Bring-Your-Own-Hardening)
- TODO Populate Wiki with Usage Notes
- TODO Audit for Hardening Opportunities(!)

-------------

### Automated Salt Installation for Pubes 4.1+ via [pestle](https://gitlab.com/pubes-os/feature/pestle_4_pubes)

![](trixie.mp4){width=100%}

-------------

### Manual Salt Installation for Pubes 4.1+

##### In dispXXXX Pube:

(Can also be downloaded via authenticated browser session)

```sh
mkdir /tmp/trixie_4_pubes && curl -sSL -H "PRIVATE-TOKEN: <INSERT_TOKEN_HERE>" "https://gitlab.com/api/v4/projects/$(printf "pubes-os/template/trixie_4_pubes" | sed 's|/|%2f|g')/repository/archive" | tar -zxf - --strip-components 1 -C /tmp/trixie_4_pubes
```

##### In dom0:

###### Install Script

```sh
qvm-run -p dispXXXX 'cat /tmp/trixie_4_pubes/install.sh' > /tmp/trixie-install.sh && bash /tmp/trixie-install.sh
```

###### Uninstall Script

```sh
qvm-run -p dispXXXX 'cat /tmp/trixie_4_pubes/uninstall.sh' > /tmp/trixie-uninstall.sh && bash /tmp/trixie-uninstall.sh
```

-------------

### Manual Installation Guide

##### In dom0:

###### 01) Clone Existing `debian-12-minimal` (`bookworm`) TemplateVM to `debian-13-minimal` (`trixie`) TemplateVM

(Can also be done within "Pubes Manager" GUI Tool)

```sh
qvm-clone debian-12-minimal debian-13-minimal && qvm-start debian-13-minimal
```

###### 02) Start `debian-13-minimal` and Launch console in DispVM

(Can also be done within "Pubes Manager" GUI Tool)

```sh
qvm-start debian-13-minimal && qvm-console-dispvm debian-13-minimal
```

##### In `debian-13-minimal` console:

###### 03) Perform Prep Update, Upgrade, Dist-Upgrade & Remove Stale Packages in New TemplateVM

```sh
apt update && apt -y upgrade && apt -y dist-upgrade && apt -y autoremove
```

###### 04) Change `apt` Release Version

```sh
sed -i 's|bookworm|trixie|g' /etc/apt/sources.list
```

###### 05) Perform Final Update, Upgrade, Dist-Upgrade, Remove Stale Packages in New TemplateVM & Shutdown

```sh
apt update && apt -y upgrade && apt -y dist-upgrade && apt -y autoremove && poweroff
```

-------------

### Licensing

Project is made freely available for individual, non-commercial use in accordance with the terms of the included [license](https://gitlab.com/pubes-os/template/trixie_4_pubes/-/raw/main/LICENSE).

Project is expressly forbidden for use in commercial environments outside of the terms of the included [license](https://gitlab.com/pubes-os/template/trixie_4_pubes/-/raw/main/LICENSE).

If you've found any of this helpful, please consider making a donation to one of the addresses below!

BTC: bc1qkd5t9aartnev3r9aj632w88vry5ychz0s4e0f7

XMR: 48kAxKy3ke6HWE5vNDPLdaeS9GjuaHs8rFV9skeqiEYQ93j8kmKg2rrJx8qnhsnkYSNwx9qn1y5vRjZdgQAFEawjDBjeCpr

-------------

Greetz & thanks to the following projects.

Tooling:  
https://www.debian.org/releases/trixie  


